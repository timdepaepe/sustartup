#!/usr/bin/env python
# ----------------------------
# Setup the swarmunit for the first time
#
# @author Tim De Paepe <tim.depaepe@gmail.com>
# ----------------------------

# import libraries
import os
from os import path
import subprocess
import time
import datetime
import sys
import fileinput

# NOT THE FIRST BOOT
if(path.exists("/home/pi/sustartup/flags/firstboot.txt")):
    print('----------------------------------------------')
    print(' Welcome to this swarm unit!')
    print('')
    print(' Developed by aifoon, Jeroen Vandesande & Tim De Paepe')
    print('----------------------------------------------')
    print('')

    # get the number of this unit
    number = open('/home/pi/number.me', 'r').read();

    # output
    print('This is Swarm Unit ' + number + '.')
    print('')

    # setup the ip address
    # print('Configuring the wireless LAN...')
    # os.system('sudo ifconfig wlan0 down')
    # os.system('sudo ifconfig wlan0 192.168.1.' + number)
    # os.system('sudo ifconfig wlan0 up')
    # time.sleep(7)

# FIRST BOOT
else:
    # welcome message
    print('----------------------------------------------')
    print(' Welcome to the FIRST boot of this swarm unit!')
    print('')
    print(' Developed by aifoon, Jeroen Vandesande & Tim De Paepe')
    print('----------------------------------------------')
    print('')

    # ask some questions
    number = input('Please enter the number of this swarm unit: ')

    # calculate the ip number
    ipNumber = str(int(number) + 10)

    # setup the hostname
    subprocess.run(['sudo', '/home/pi/sustartup/change_hostname.sh', 'rasp' + ipNumber])

    # change the static ip address
    subprocess.run(['sudo', '/home/pi/sustartup/change_static_ip.sh', ipNumber])

    # write the number of this unit
    with open("/home/pi/" + "number.me", mode='a') as file:
        file.write(number)

    # write the install flag
    with open("/home/pi/sustartup/flags/" + "firstboot.txt", mode='a') as file:
        file.write('The first boot was recorded at %s.' % (datetime.datetime.now()))

    # reboot the system
    print('')
    print('We will reboot the system...')
    time.sleep(3)
    os.system('sudo shutdown -r now')