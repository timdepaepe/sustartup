#!/bin/bash

# args:
# $1 - the last part of the ip address

ip="$1"

cp /home/pi/sustartup/dhcpcd.conf.original /home/pi/sustartup/dhcpcd.conf
sed -i -r "s/inform 192.168.1.XX/inform 192.168.1.${ip}/" /home/pi/sustartup/dhcpcd.conf
mv /home/pi/sustartup/dhcpcd.conf /etc/dhcpcd.conf
